const config = require("./config/config.js");
var express = require('express');
var mysql = require('mysql');
var bodyParser = require('body-parser');
var app = express();

global.dbConfig = mysql.createPool({
  host: config.serverName,
  user: config.userName,
  database: config.databaseName,
  password: config.password,
  port: 3306,
  waitForConnections: true,
  connectionLimit: 20,
  queueLimit: 0
});


// body-parser configuration
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}));

//Enable cors origin
var cors = require('cors');
app.use(cors({
  origin: true
}));

const stockDetails = require("./routes/stockDetails");
app.use("/api/stock", stockDetails);

const server = app.listen(process.env.PORT || 8000, function() {
  console.log("API is running in 8000");
});