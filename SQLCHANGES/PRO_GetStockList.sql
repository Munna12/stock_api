CREATE DEFINER=`root`@`localhost` PROCEDURE `PRO_GetStockList`()
BEGIN

	SELECT 
			id,
			name AS companyName,
			current_market_price AS currentMarketPrice,
			market_cap AS marketCap,
			stock_pe AS stockPE,
			roce,
			roe_previous_annum AS roePreviousAnnum,
			debt_to_equity AS debtToEquity,
            eps,
            reserves,
            debt,
            dividend_yield as dividendYield
	FROM  stock_information s
	ORDER BY id DESC;
    
END