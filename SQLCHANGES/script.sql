CREATE TABLE `stock_information` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `current_market_price` varchar(100) DEFAULT NULL,
  `market_cap` varchar(100) DEFAULT NULL,
  `stock_pe` varchar(100) DEFAULT NULL,
  `dividend_yield` varchar(100) DEFAULT NULL,
  `roce` varchar(100) DEFAULT NULL,
  `roe_previous_annum` varchar(100) DEFAULT NULL,
  `debt_to_equity` varchar(100) DEFAULT NULL,
  `eps` varchar(100) DEFAULT NULL,
  `reserves` varchar(100) DEFAULT NULL,
  `debt` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) 